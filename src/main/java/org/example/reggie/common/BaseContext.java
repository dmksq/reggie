package org.example.reggie.common;

public class BaseContext<C> {
    public static ThreadLocal<Long> threadLocal=new ThreadLocal<>();

    public static void set(Long id){
        threadLocal.set(id);
    }

    public static Long get(){
        return threadLocal.get();
    }
}
