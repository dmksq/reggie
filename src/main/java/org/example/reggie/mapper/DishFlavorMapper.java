package org.example.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.reggie.entity.DishFlavor;
import org.example.reggie.service.DishFlavorService;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {
}
