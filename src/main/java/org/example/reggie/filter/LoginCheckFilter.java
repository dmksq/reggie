package org.example.reggie.filter;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.example.reggie.common.BaseContext;
import org.example.reggie.common.R;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "loginCheckFilter",urlPatterns = "/*")
@Slf4j
public class LoginCheckFilter implements Filter {
    public static final AntPathMatcher PATH_MATCHER=new AntPathMatcher();
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request=(HttpServletRequest) servletRequest;
        HttpServletResponse response=(HttpServletResponse) servletResponse;
        log.info(request.getRequestURI());
        //定义放行请求
        String[] urls={
                "/backend/**",
                "/front/**",
                "/employee/login",
                "/employee/logout"
        };
        //匹配看是否直接放行
        boolean flag=urlMatch(urls,request.getRequestURI());
        if(flag){
            filterChain.doFilter(request,response);
            return;
        }
        //查询登录状态
        if(request.getSession().getAttribute("employee")!=null){
            BaseContext.set((Long) request.getSession().getAttribute("employee"));
            filterChain.doFilter(request,response);
        }
        else{
//            log.info("{}",request.getSession().getAttribute("employee"));
            response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
            return;
        }

    }
    public  boolean urlMatch(String[] urls,String url){
        for(int i=0;i<urls.length;++i){
            if(PATH_MATCHER.match(urls[i],url)){
                return true;
            }
        }
        return false;
    }

}
